<?php get_header()?>

    <main role="main" class="container">
	
	<div class="container">
		<div class="row">
		
				<?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post();
					get_template_part( 'content', get_post_format() ); 
				endwhile; endif;	
				?>
				
		</div>
	</div>

    </main> <!-- /.container -->

<?php get_footer(); ?>
