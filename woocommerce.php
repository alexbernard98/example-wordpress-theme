<?php get_header()?>

    <main role="main" class="container">
	
	<div class="container">
		<div class="row">
		
				<?php woocommerce_content(); ?>
				
		</div>
	</div>

    </main><!-- /.container -->

<?php get_footer(); ?>
